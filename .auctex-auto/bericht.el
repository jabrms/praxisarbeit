(TeX-add-style-hook
 "bericht"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("scrreprt" "paper=a4" "listof=totoc" "ngerman")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("csquotes" "autostyle" "german=guillemets") ("biblatex" "style=verbose") ("acronym" "printonlyused") ("varioref" "nospace") ("hyperref" "hidelinks") ("scrlayer-scrpage" "headsepline")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "scrreprt"
    "scrreprt10"
    "babel"
    "csquotes"
    "fontspec"
    "microtype"
    "tabularx"
    "biblatex"
    "acronym"
    "tikz"
    "graphicx"
    "varioref"
    "hyperref"
    "scrlayer-scrpage")
   (LaTeX-add-labels
    "cha:glossary"
    "cha:einleitung"
    "sec:firmenportrait"
    "sec:aufg--arbe"
    "sec:uberblick"
    "cha:search-engine-optim"
    "sec:ausg-und-probl"
    "sec:werkzeuge"
    "sec:shopify"
    "sec:seobility"
    "sec:ide"
    "sec:versionskontrolle"
    "sec:vorgehensweise"
    "sec:entf-toter-links"
    "sec:bilder"
    "sec:seitenstruktur"
    "fig:prod-struct"
    "sec:erst-von-inhalt"
    "cha:funkt-erw"
    "sec:produkt-finder"
    "fig:devices"
    "sec:anforderungen-prod-find"
    "sec:ergebnisseiten-anf"
    "sec:planung-prod-find"
    "sec:geme-bei-best"
    "sec:struktur"
    "fig:pf-struct"
    "sec:ergebnisseiten-plan"
    "sec:umsetzung-prod-find"
    "sec:frage-antw"
    "fig:pf-design"
    "sec:ergebnisseiten"
    "sec:flachenrechner"
    "sec:anforderungen-wand-rech"
    "sec:funktionen"
    "sec:fr-ui"
    "sec:planung-wand-rech"
    "sec:umsetzung-wand-rech"
    "sec:anzahl-und-flach"
    "sec:wand-und-deck"
    "cha:weitere-aufgaben"
    "sec:designoptimierungen"
    "sec:autom-preis"
    "sec:ubersicht-preise"
    "sec:planung-preise"
    "sec:implementierung"
    "cha:ergebnisse"
    "sec:seo"
    "sec:funkt-erwe"
    "sec:weitere-aufgaben"
    "cha:zusamm-und-ausbl")
   (LaTeX-add-bibliographies
    "bibliography")
   (LaTeX-add-acronyms
    "api"
    "gui"
    "rest"
    "seo"
    "serp"
    "sku"
    "ui"
    "url"
    "ux"))
 :latex)

