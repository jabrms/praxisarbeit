(TeX-add-style-hook
 "bibliography"
 (lambda ()
   (LaTeX-add-bibitems
    "hochschule_worms_fachbereich_informatik_anleitung_2020"
    "kocher_brown_8_2019"
    "beus_wieso_2020"
    "kustov_council_nodate"
    "forbes_media_2020_nodate"
    "brightedge_research_technologies_inc_organic_nodate"
    "google_uber_nodate"
    "zenner_blick_2020"
    "collins_seo_2020"
    "ebner_seo_2019"
    "noauthor_frequently_nodate"))
 :bibtex)

