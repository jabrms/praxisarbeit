\babel@toc {ngerman}{}
\beamer@sectionintoc {1}{Einleitung}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Firmenportrait}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Aufgabenstellung \& Arbeitsgebiet}{4}{0}{1}
\beamer@sectionintoc {2}{SEO}{7}{0}{2}
\beamer@subsectionintoc {2}{1}{Problemstellung}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{Werkzeuge}{8}{0}{2}
\beamer@subsectionintoc {2}{3}{Vorgehensweise}{12}{0}{2}
\beamer@sectionintoc {3}{Funktionelle Erweiterung}{13}{0}{3}
\beamer@subsectionintoc {3}{1}{Produkt-Finder}{13}{0}{3}
\beamer@subsectionintoc {3}{2}{Flächenrechner}{18}{0}{3}
\beamer@sectionintoc {4}{Weitere Aufgaben}{21}{0}{4}
\beamer@subsectionintoc {4}{1}{UX-Optimierungen}{21}{0}{4}
\beamer@subsectionintoc {4}{2}{Automatisierte Preisanpassung}{22}{0}{4}
\beamer@sectionintoc {5}{Ergebnisse}{23}{0}{5}
\beamer@sectionintoc {6}{Zusammenfassung}{24}{0}{6}
