\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{4}{chapter*.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Abkürzungsverzeichnis}{5}{chapter*.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{6}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Firmenportrait}{6}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Aufgabenstellung \& Arbeitsgebiet}{6}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Überblick}{7}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}SEO}{8}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Ausgangssituation und Problemstellung}{8}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Werkzeuge}{9}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}\emph {Shopify}}{9}{subsection.2.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}\emph {Seobility}}{10}{subsection.2.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}IDE}{10}{subsection.2.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.4}Versionskontrolle}{10}{subsection.2.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Vorgehensweise}{11}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Entfernung toter Links}{11}{subsection.2.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Bilder}{11}{subsection.2.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Seitenstruktur}{12}{subsection.2.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.4}Anpassung und Erstellung von Inhalten}{13}{subsection.2.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Blog}{13}{section*.11}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Produktseiten}{13}{section*.12}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Funktionelle Erweiterung des Onlineshops}{15}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Produkt-Finder}{15}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Anforderungen}{15}{subsection.3.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Sichtbarkeit}{15}{section*.13}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Responsive \ac {ui}}{15}{section*.14}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Funktionalität}{15}{section*.15}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Benutzbarkeit}{16}{section*.16}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Anpassbarkeit}{16}{section*.17}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Ergebnisseiten}{16}{section*.18}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Planung}{16}{subsection.3.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Gemeinsamkeiten bei bestehenden Lösungen}{17}{section*.19}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Frage-Antwort-Aufbau}{17}{section*.20}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Bilder}{17}{section*.21}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Fortschrittsanzeige}{17}{section*.22}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Struktur}{17}{section*.23}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Ergebnisseiten}{18}{section*.24}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}Umsetzung}{18}{subsection.3.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Frage-/Antwortstruktur}{18}{section*.26}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Ergebnisseiten}{20}{section*.27}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Flächenrechner}{20}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Anforderungen}{21}{subsection.3.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Funktionen}{21}{section*.28}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Berechnung der abgedeckten Fläche anhand der gewählten Produktmenge}{21}{section*.29}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Berechnung der Produktmenge anhand der abzudeckenden Fläche}{21}{section*.30}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Berechnung der Wand- und Deckenfläche anhand der Grundfläche eines Raumes}{21}{section*.31}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline \ac {ui}}{21}{section*.32}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Planung}{21}{subsection.3.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Umsetzung}{22}{subsection.3.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Anzahl- und Flächenberechnung}{22}{section*.33}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Wand- und Deckenflächenberechnung}{22}{section*.34}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Weitere Aufgaben}{24}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}UX-Optimierungen}{24}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Login-/Logout}{24}{section*.35}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Warenkorbwert}{24}{section*.36}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Chat-Buttons}{24}{section*.37}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Empfehlungskennzeichnung}{24}{section*.38}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Automatisierte Preisanpassung}{25}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Übersicht}{25}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Planung}{25}{subsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Implementierung}{26}{subsection.4.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Ergebnisse}{27}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}SEO}{27}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Funktionelle Erweiterung}{28}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Weitere Aufgaben}{28}{section.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Zusammenfassung und Ausblick}{29}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{31}{chapter*.40}%
